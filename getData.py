import urllib.request
import unicodecsv as csv
from xml.dom import minidom

nuts = [["CZ0100", "Praha"],
        ["CZ0201", "Benešov"],
        ["CZ0202", "Beroun"],
        ["CZ0203", "Kladno"],
        ["CZ0204", "Kolín"],
        ["CZ0205", "Kutná Hora"],
        ["CZ0206", "Mělník"],
        ["CZ0207", "Mladá Boleslav"],
        ["CZ0208", "Nymburk"],
        ["CZ0209", "Praha-východ"],
        ["CZ020A", "Praha-západ"],
        ["CZ020B", "Příbram"],
        ["CZ020C", "Rakovník"],
        ["CZ0311", "České Budějovice"],
        ["CZ0312", "Český Krumlov"],
        ["CZ0313", "Jindřichův Hradec"],
        ["CZ0314", "Písek"],
        ["CZ0315", "Prachatice"],
        ["CZ0316", "Strakonice"],
        ["CZ0317", "Tábor"],
        ["CZ0321", "Domažlice"],
        ["CZ0322", "Klatovy"],
        ["CZ0323", "Plzeň-město"],
        ["CZ0324", "Plzeň-jih"],
        ["CZ0325", "Plzeň-sever"],
        ["CZ0326", "Rokycany"],
        ["CZ0327", "Tachov"],
        ["CZ0411", "Cheb"],
        ["CZ0412", "Karlovy Vary"],
        ["CZ0413", "Sokolov"],
        ["CZ0421", "Děčín"],
        ["CZ0422", "Chomutov"],
        ["CZ0423", "Litoměřice"],
        ["CZ0424", "Louny"],
        ["CZ0425", "Most"],
        ["CZ0426", "Teplice"],
        ["CZ0427", "Ústí nad Labem"],
        ["CZ0511", "Česká Lípa"],
        ["CZ0512", "Jablonec nad Nisou"],
        ["CZ0513", "Liberec"],
        ["CZ0514", "Semily"],
        ["CZ0521", "Hradec Králové"],
        ["CZ0522", "Jičín"],
        ["CZ0523", "Náchod"],
        ["CZ0524", "Rychnov nad Kněžnou"],
        ["CZ0525", "Trutnov"],
        ["CZ0531", "Chrudim"],
        ["CZ0532", "Pardubice"],
        ["CZ0533", "Svitavy"],
        ["CZ0534", "Ústí nad Orlicí"],
        ["CZ0631", "Havlíčkův Brod"],
        ["CZ0632", "Jihlava"],
        ["CZ0633", "Pelhřimov"],
        ["CZ0634", "Třebíč"],
        ["CZ0635", "Žďár nad Sázavou"],
        ["CZ0641", "Blansko"],
        ["CZ0642", "Brno-město"],
        ["CZ0643", "Brno-venkov"],
        ["CZ0644", "Břeclav"],
        ["CZ0645", "Hodonín"],
        ["CZ0646", "Vyškov"],
        ["CZ0647", "Znojmo"],
        ["CZ0711", "Jeseník"],
        ["CZ0712", "Olomouc"],
        ["CZ0713", "Prostějov"],
        ["CZ0714", "Přerov"],
        ["CZ0715", "Šumperk"],
        ["CZ0721", "Kroměříž"],
        ["CZ0722", "Uherské Hradiště"],
        ["CZ0723", "Vsetín"],
        ["CZ0724", "Zlín"],
        ["CZ0801", "Bruntál"],
        ["CZ0802", "Frýdek-Místek"],
        ["CZ0803", "Karviná"],
        ["CZ0804", "Nový Jičín"],
        ["CZ0805", "Opava"],
        ["CZ0806", "Ostrava-město"]]
years = [2013, 2017]
url = "https://volby.cz/pls/ps"
url_end = "/vysledky_okres?nuts="
parties2013 = {1: 'ČSSD', 4: 'TOP 09', 6: 'ODS', 11: 'KDU-ČSL', 20: 'ANO', 21: 'KSČM', 17: 'Úsvit'}
parties2017 = {1: 'ODS', 4: 'ČSSD', 7: 'Starostové', 8: 'KSČM', 15: 'Piráti', 20: 'TOP 09', 21: 'ANO', 24: 'KDU-ČSL',
               29: 'SPD'}
partiesAll = ['ODS', 'ČSSD', 'Starostové', 'KSČM', 'Piráti', 'TOP 09', 'ANO', 'KDU-ČSL', 'SPD', 'Úsvit']

result = []
for nut in nuts:
    for year in years:
        print(url + str(year) + url_end + nut[0])

        url_str = url + str(year) + url_end + nut[0]
        f = urllib.request.urlopen(url_str)
        xml_str = f.read()
        xmldoc = minidom.parseString(xml_str)

        okres = xmldoc.getElementsByTagName('OKRES')
        naz_okres = okres[0].attributes['NAZ_OKRES'].value
        itemlist = xmldoc.getElementsByTagName('OBEC')
        for obec in itemlist:
            naz_obec = obec.attributes['NAZ_OBEC'].value
            ucast = obec.getElementsByTagName('UCAST')
            okrsky_celkem = ucast[0].attributes['OKRSKY_CELKEM'].value
            zapsani_volici = ucast[0].attributes['ZAPSANI_VOLICI'].value
            ucast_proc = ucast[0].attributes['UCAST_PROC'].value
            platne_hlasy = ucast[0].attributes['PLATNE_HLASY_PROC'].value
            hlasy = obec.getElementsByTagName('HLASY_STRANA')
            parties_results = {}
            for hlas in hlasy:
                strana_cislo = int(hlas.attributes['KSTRANA'].value)
                if year == 2013 and strana_cislo in parties2013.keys():
                    party = parties2013[strana_cislo]
                    parties_results[party] = hlas.attributes['PROC_HLASU'].value
                if year == 2017 and strana_cislo in parties2017.keys():
                    party = parties2017[strana_cislo]
                    parties_results[party] = hlas.attributes['PROC_HLASU'].value
            mylist = [str(naz_okres), str(naz_obec), str(year), str(okrsky_celkem), str(zapsani_volici),
                      str(ucast_proc), str(platne_hlasy)]
            for party in partiesAll:
                if party in parties_results.keys():
                    mylist.append(str(parties_results[party]))
                else:
                    mylist.append(str(0))
            result.append(mylist)

with open("data.csv", 'wb') as myfile:
    #wr = csv.writer(myfile, delimiter=';')
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    for row in result:
        wr.writerow(row)
