# Analýza voleb do Poslanecké sněmovny Parlamentu ČR
## Seminární práce pro předmět 4IZ460 – Pokročilé přístupy k dobývání znalostí z databází

Seznam souborů:  
1.   getData.py - python skript pro stažení souborů z Českého statistického úřadu  
2.   data.csv - stažená data  
3.   vysledky.xlsx - stažená data doplněná o údaje z MSPV a některá předzpracování  
4.   vysledky_csv.csv - datový soubor pro Lisp-Miner  
5.   vysledky_csv.Data.mdb - metadata pro Lisp-Miner  
6.   vysledky_csv.LMMB.mdb - metadata pro Lisp-Miner  